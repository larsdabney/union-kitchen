<?php get_header(); ?>

<?php if (get_field('media_type') === 'image') : ?>

<div class='page-media image' style='background-image:url(<?= get_field('header_image') ?>)'>

    <?php if (get_field('header_text')) : ?><h2><?= get_field('header_text') ?></h2><?php endif; ?>

    <div class='media-overlay'></div>

</div>

<?php elseif (get_field('media_type') === 'video') : // TODO here and in index.php ?>

<div class='page-media video' style='background-image:url(<?= get_field('header_image') ?>)'>

    <?php if (get_field('header_text')) : ?><h2><?= get_field('header_text') ?></h2><?php endif; ?>

    <video autoplay loop muted="true" class='page-video'>
        <source src="<?= get_field('webm') ?>" type="video/webm">
        <source src="<?= get_field('mp4') ?>" type="video/mp4">
    </video>

    <div class='media-overlay'></div>

</div>

<?php endif; ?>

<section class='inner-page' id="inner-page">

    <!-- <div class="center"> -->
    <!--  -->
    <!--     <h1 class="alt page&#45;header"><?php the_title() ?></h1> -->
    <!--  -->
    <!-- </div> -->

    <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

    <div class="entry">

<?php
if( have_rows('section_content') ):

     // loop through the rows of data
    while ( have_rows('section_content') ) : the_row();

        get_template_part('layouts/' . get_row_layout());

    endwhile;

else :

    // no layouts found

endif;
?>

    </div>

    <?php endwhile; endif; ?>

</section>

<?php get_footer(); ?>
