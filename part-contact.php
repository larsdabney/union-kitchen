
<section class="get-in-touch" id="get-in-touch">
    <form id="subscribe" class='subscribe corset' action='/' method='post'>
        <fieldset>
          <div class="disabler"></div>
          <div class='row'>
              <span class="email-callout"><?= get_field("email_callout_text", "options") ?></span>
              <div class='field-controller'>
                  <div class='input-wrapper'>
                      <input
                          type="email"
                          name="email"
                          autocomplete="off"
                          placeholder="you@email.com"
                      />
                  </div>
                  <div class="btn-wrapper">
                      <button type="submit">Submit</button>
                  </div>
              </div>
          </div>
        </fieldset>
    </form>


<?php /* ?>
<div class="wrapper">
<?php try {
    $field_return = get_field('map_locations', 'options');
    echo '<script>console.log("' . print_r($field_return) . '");</script>';
    $locations = json_encode($field_return);
} catch (Exception $error) {
    $locations = json_encode(array());
}
if ($locations === null) {
    $locations = array();
}
echo "<script>window.uklocations = $locations;</script>";
?>

        <div class="map"></div>
            </div>
        <?php */ ?>

</section>
