<?php get_header(); ?>

<?php if (get_field('media_type') === 'image') : ?>

<div class='page-media image' style='background-image:url(<?= get_field('header_image') ?>)'>

    <?php if (get_field('header_text')) : ?><h2><?= get_field('header_text') ?></h2><?php endif; ?>

    <div class='media-overlay'></div>

</div>

<?php elseif (get_field('media_type') === 'video') : // TODO ?>

<div class='page-media video' style='background-image:url(<?= get_field('header_image') ?>)'>

    <?php if (get_field('header_text')) : ?><h2><?= get_field('header_text') ?></h2><?php endif; ?>

    <?php if (get_field('header_text')) : ?><h2><?= get_field('header_text') ?></h2><?php endif; ?>

    <div class='media-overlay'></div>

</div>

<?php endif; ?>

<section class="corset" id="blog">
  <aside class="sidebar">
    <?php if ( is_single() ) : ?>
    <a href="/blog/" class="back-to-blog"><i class='icon-left-circle'></i> Back to the blog</a>
    <?php endif; ?>
    <form role="search" class='search-box' action="<?php echo site_url('/'); ?>" method="get">
      <input type="search" name="s" placeholder="Search all posts"/>
      <small>press enter to submit</small>
    </form>
    <?php get_sidebar(); ?>
  </aside>
  <div class="posts">
    <?php if ( is_archive() && ! is_tag() && ! is_category() ) : ?>
    <h5 class="blog-description">
      <?php
      if ( is_day() ) :
        get_the_date();
      elseif ( is_month() ) :
        get_the_date('f y');
      elseif ( is_year() ) :
        get_the_date('y');
      else :
        echo "Archives";
      endif;
      ?>
    </h5>
    <?php endif; ?>
    <?php if ( is_tag() ) : ?>
    <h5 class="blog-description">
      Displaying posts taged with <em><?= single_tag_title( '', false ); ?></em>.
    </h5>
    <?php endif; ?>
    <?php if ( is_category() ) : ?>
    <h5 class="blog-description">
      Displaying posts categorized under <em><?= single_cat_title( '', false ); ?></em>.
    </h5>
    <?php endif; ?>
    <?php if ( is_search() ) : ?>
    <h5 class="blog-description">
      Displaying search results for <em><?= get_search_query() ?></em>
    </h5>
    <?php endif; ?>


    <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
    <?php get_template_part("content", "post"); ?>
    <?php endwhile; endif; ?>
    <?php uk_numeric_posts_nav(); ?>
  </div>
</div>
<?php get_template_part('part', 'contact'); ?>
<?php get_footer(); ?>
