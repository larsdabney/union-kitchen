<?php
header('Content-Type: application/json');
$isAjax = isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) === 'xmlhttprequest';
if ( ! $isAjax ) { exit; }

require 'libs/EmmaPHP-master/src/Emma.php';
$account_id = 1758344;
$public_key = '77830eb2710099785c64';
$private_key = '4a06b8f11ca1b4a5c513';
$emma = new Emma($account_id, $public_key, $private_key);

$data = array();

if (!isset($_POST['email']) ) {
  $data['message'] = "You must provide your email.";
} else {
  $email = $_POST['email'];
  try {
    $data['message'] = "You have been added to the newsletter! Please check your email to confirm. Thank you!";
    $member = array();
    $member['email'] = $email;
    $data['res'] = $emma->membersAddSingle($member);
  } catch (Emma_Invalid_Response_Exception $e ) {
    if ( $e->getMessage() ) {
      $data['message'] = $e->getMessage();
    } else {
      $data['message'] = "An unknown error has occured.";
    }
  }
}

echo json_encode($data);
die();
