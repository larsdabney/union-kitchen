<!DOCTYPE html>
<!--[if IE 7]>
<html class="ie ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 7) | !(IE 8) ]><!-->
<html <?php language_attributes(); ?>>
<!--<![endif]-->
<head>
<meta charset="<?php bloginfo('charset'); ?>" />
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title><?php bloginfo('name'); ?> | <?php is_front_page() ? bloginfo('description') : wp_title(''); ?></title>
<script src="<?php echo site_url(); ?>/wp-content/themes/union-kitchen/assets/js/vendor/modernizr.min.js"></script>
<link rel="profile" href="http://gmpg.org/xfn/11" />
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
<link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo('stylesheet_url'); ?>" />
<?= Rye::stylesheet() ?>
<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />

<?php wp_head(); ?>

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-56531220-1', 'auto');
  ga('send', 'pageview');

</script>
</head>
<body <?php body_class(); ?>>
<?php if (is_front_page()) : ?>
<div style="background-color:#333;position:fixed;top:0;left:0;right:0;bottom:0;z-index:500;" id="loader"><div></div><i></i></div>
<?php endif; ?>
<div class="main-nav <?php if (get_field('media_type') === 'video') { ?> video-page <?php } ?> sticky <?php if ( get_field("mobile_header_image", "options") ) :  ?> header-mobile-image <?php else: ?> no-header-mobile-image <?php endif; ?>">
  <h1><a href="/">Union Kitchen | Food Incubator</a></h1>
  <a href="javascript:void(0)" class="tablet-nav-btn"><i class="icon-menu-1"></i></a>
  <?php wp_nav_menu( array( 'theme_location' => 'header-menu', 'container' => 'nav', 'menu_class' => 'nav' ) ); ?>
</div>
<div class="body">
<?php if ( is_front_page() && ! is_home() ) : // If landing page, but not blog. ?>
<header
    class="header<?php if ( get_field("mobile_header_image", "options") ) :  ?> header-mobile-image <?php else: ?> no-header-mobile-image <?php endif; ?> "
    data-vide-bg="<?php echo get_stylesheet_directory_uri(); ?>/assets/video/create_1"
    data-vide-options="position: 0% 0%, muted: true">
      <?php if ( get_field("mobile_header_image", "options") ) :  ?>
          <div class="mobile-img-div" style="background-image:url(<?php the_field("mobile_header_image", "options"); ?>)"></div>
      <?php endif; ?>
      <div class="center get-started">
        <h1><a href="/">Union Kitchen | Food Incubator</a></h1>
		  <div class="landing-tag">Building successful<br/>food businesses</div>
      </div>
      <div class="main-nav">
        <a href="javascript:void(0)" class="tablet-nav-btn"><i class="icon-menu-1"></i></a>
        <?php wp_nav_menu( array( 'theme_location' => 'header-menu', 'container' => 'nav', 'menu_class' => 'nav' ) ); ?>
      </div>
    </div>
  </div>
</header>
<?php endif; ?>
<main <?php if (get_field('media_type') === 'video') { ?> class="video-page" <?php } ?> >
