<?php get_template_part('part', 'contact'); ?>
</main>
<footer class="footer">
    <div class='corset'>
        <div class='first branding'>
            <i class="made-in-uk"></i>
            <img src='/wp-content/themes/union-kitchen/assets/img/b-corp.png' width='30'>
        </div>
        <div class='second menu'>
            <?php wp_nav_menu( array( 'theme_location' => 'footer-menu', 'container' => 'nav' ) ); ?>
        </div>
        <div class='third social'>
            <div class='social-contain'>
                <?php if ( get_field('facebook_url', 'options') ) : ?>
                <a href='<?= get_field('facebook_url', 'options') ?>'>
                    <i class='fa fa-facebook'></i>
                </a>
                <?php endif; ?>
                <?php if ( get_field('twitter_url', 'options') ) : ?>
                <a href='<?= get_field('twitter_url', 'options') ?>'>
                    <i class='fa fa-twitter'></i>
                </a>
                <?php endif; ?>
                <?php if ( get_field('instagram_url', 'options') ) : ?>
                <a href='<?= get_field('instagram_url', 'options') ?>'>
                    <i class='fa fa-instagram'></i>
                </a>
                <?php endif; ?>
                <?php if ( get_field('youtube_url', 'options') ) : ?>
                <a href='<?= get_field('youtube_url', 'options') ?>'>
                    <i class='fa fa-youtube-square'></i>
                </a>
                <?php endif; ?>
            </div>

        </div>

        <div class="contact-info corset">
          <?php the_field('contact_info', 'options'); ?>
        </div>

    </div>
    <div class="uk-addresses">
      <div class="address-col">
        <h5><?php the_field('location_1','options'); ?></h5>
        <a class="address-link" href="https://maps.google.com/?q=1369+New+York+Ave+NE+Washington,+DC+20002&entry=gmail&source=g>">
          <p><?php the_field('address_1','options'); ?></p>
        </a>
      </div>
      <div class="address-col">
        <h5><?php the_field('location_2','options'); ?></h5>
        <a class="address-link" href="https://maps.google.com/?q=9th+Street1251+9th+Street+NW+Washington,+DC+20001&entry=gmail&source=g">
          <p><?php the_field('address_2','options'); ?></p>
        </a>
      </div>
      <div class="address-col">
        <h5><?php the_field('location_3','options'); ?></h5>
        <a class="address-link" href="https://maps.google.com/?q=538+3rd+Street+NW+Washington,+DC+20002&entry=gmail&source=g">
          <p><?php the_field('address_3','options'); ?></p>
        </a>
      </div>
    </div>
    <div class="copyright">
        <span>&copy; Union Kitchen 2016</span>
    </div>
</footer>
<?php wp_footer(); ?>
<script>
(function(b,o,i,l,e,r){b.GoogleAnalyticsObject=l;b[l]||(b[l]=
function(){(b[l].q=b[l].q||[]).push(arguments)});b[l].l=+new Date;
e=o.createElement(i);r=o.getElementsByTagName(i)[0];
e.src='//www.google-analytics.com/analytics.js';
r.parentNode.insertBefore(e,r)}(window,document,'script','ga'));
ga('create','UA-46192505-1');ga('send','pageview');
</script>
</div><!-- /.body -->
</body>
</html>
