<?php
/**
 *  Include Rye.
 */
require_once 'libs/rye.php';

/**
 * Set domains and any other constants here.
 */
define("PRODUCTION_SERVER", "unionkitchendc.com");
define("STAGING_SERVER", "union-kitchen.web.lev-interactive.com");

/**
 * Set the enviornment property. This will determine which version of the
 * assets will be used.
 */
if (stripos($_SERVER['SERVER_NAME'], PRODUCTION_SERVER) !== false) {
    Rye::$enviornment = Rye::PRODUCTION;
} else if (stripos($_SERVER['SERVER_NAME'], STAGING_SERVER) !== false) {
    Rye::$enviornment = Rye::STAGING;
} else {
    Rye::$enviornment = Rye::DEVELOPMENT;
}

/**
 *  Site configurations.
 */
Rye::init(array(

  /**
   *  Place JavaScripts in footer. This tends to break some plugins
   *  because they sadly put jQuery in the DOM. Enable with care.
   *  http://developer.yahoo.com/performance/rules.html#js_bottom
   */
  'place_javascript_in_footer' => true,



  /**
   *  Path to JavaScript files.
   *  http://codex.wordpress.org/Function_Reference/wp_register_script
   */
  'javascripts' => array(
    'maps' => '//maps.googleapis.com/maps/api/js?v=3.exp&sensor=false',
    'jquery' => get_bloginfo('template_directory').'/assets/js/vendor/jquery-2.1.0.min.js',
    'lodash' => 'https://cdnjs.cloudflare.com/ajax/libs/lodash.js/3.10.1/lodash.min.js',
    'plugins' => get_bloginfo('template_directory').'/assets/js/vendor/plugins.min.js',
    'owl' => get_bloginfo('template_directory').'/assets/js/vendor/owl.carousel.min.js',
    'vide' => get_bloginfo('template_directory').'/assets/js/vendor/jquery.vide.min.js',
    'instafeed' => get_bloginfo('template_directory').'/assets/js/instafeed.min.js',
     'main' => get_bloginfo('template_directory').'/assets/dist/'.Rye::projectName().'.all.min.js?bust=1.0.2',
  ),



  /**
   *  Path to JavaScript files.
   *  http://codex.wordpress.org/Function_Reference/add_image_size
   *
   *  '<image-size-name>' => array(<width>, <height>, <crop>)
   */
  'image_sizes' => array(
    /*
    'featured_post'  => array(500, 500, false),
    'featured_article' => array(200, 200, true)
    */
  ),



  /**
   *  Declare custom menu regions.
   *  http://codex.wordpress.org/Function_Reference/register_nav_menus
   */
  'menus' => array(
    'header-menu' => "Main Navigation",
    'footer-menu' => "Footer Navigation"
  ),



  /**
   *  Declare theme features.
   *  http://codex.wordpress.org/Function_Reference/add_theme_support
   *
   *  '<feature>' => array('<arg>', '<arg>')
   */
  'theme_support' => array(
    'post-thumbnails' => array('post', 'page')
  ),



  /**
   *  Declare "widgetized" regions.
   *  http://codex.wordpress.org/Function_Reference/register_sidebar
   */
  'widgetized_regions' => array(
    array(
      'name'          => 'Blog Sidebar',
      'description'   => 'The sidebar for the blog.',
      'id'            => 'blog-sidebar',
      'before_title'  => '<h4>',
      'after_title'   => '</h4>',
      'before_widget' => '<div id="%1$s" class="widget %2$s">',
      'after_widget'  => '</div>',
    )
  ),



  /**
   *  Declare custom post types.
   *  http://codex.wordpress.org/Function_Reference/register_post_type
   */
  'post_types' => array(
      'member' => array(
            'labels' => array(
                'name' => 'Members',
                'singular_name' => 'Member'
            ),
            'public' => true,
            'rewrite' => array(
                'slug' => false
            ),
            'publicly_queryable' => false,
            'has_archive' => false
        ),
    'events' => array(
      'labels'     => array('name' => 'Events'),
      'public'     => false,
      'publicly_queryable' => false,
      'show_ui'    => true,
      'show_in_menu'   => true,
      'query_var'    => true,
      'rewrite'    => true,
      'capability_type'  => 'post',
      'has_archive'  => true,
      'hierarchical'   => false,
      'menu_position'  => 4,
      'supports'     => array('title')
    )
  ),



  /**
   *  Declare custom taxonomies.
   *  http://codex.wordpress.org/Function_Reference/register_taxonomy
   */
  'taxonomies' => array(
    /*
    array(
      'tax_name', 'postype_name', array(
      'hierarchical'  => false,
      'labels'    => array('name' => '<Tax Name>'),
      'show_ui'   => true,
      'query_var'   => true,
      'rewrite'   => array('slug' => 'tax-name'),
      )
    ),
    array(
      'tax_name', 'postype_name', array(
      'hierarchical'  => false,
      'labels'    => array('name' => '<Tax Name>'),
      'show_ui'   => true,
      'query_var'   => true,
      'rewrite'   => array('slug' => 'tax-name'),
      )
    )
    */
  )
));

/**
 * Setting up stylesheet for edits... because JESUS what were these guys thinking
 */
add_action('init', 'lf_register_styles');

function lf_register_styles() {
	wp_register_style('style-edits', get_template_directory_uri() . '/assets/css/style-edits.css', array(), 1.2, 'all');
}

add_action('wp_enqueue_scripts', 'lf_enqueue_styles');

function lf_enqueue_styles() {
	wp_enqueue_style('style-edits');
}

/**
 *  Filters.
 *  Miscellaneous theme specific utility filters.
 */

/**
 *  Filter text through the the_content filter. Userful outside the loop.
 *  http://codex.wordpress.org/Function_Reference/the_content#Alternative_Usage
 *
 *  apply_filters('wp_content', $str);
 */
add_filter('wp_content', function ($str) {
   $content = apply_filters('the_content', $str);
   $content = str_replace(']]>', ']]&gt;', $content);
   return $content;
}, 10, 1);


/**
 *  Truncate text by words. Note: This also strips html tags.
 *  https://bitbucket.org/ellislab/codeigniter/src
 *
 *  apply_filters('truncate_by_words', $longstr, 20, '...');
 */
add_filter('truncate_by_words', function ($str, $limit = 100, $end_char = '&#8230;') {
   if (trim($str) == '')
     return strip_tags($str);

   preg_match('/^\s*+(?:\S++\s*+){1,'.(int) $limit.'}/', $str, $matches);

   if (strlen($str) == strlen($matches[0]))
     $end_char = '';

   return strip_tags(rtrim($matches[0]).$end_char);
}, 10, 3);


/**
 *  Theme specific methods.
 *  Other methods which make the theme function.
 */

function uk_get_meta() {
  if ( is_single() ) {
?>
  <div class="meta">
    <div class='tag-list'>
      <?php echo get_the_tag_list('<span>Related </span>',', ',''); ?>
    </div>
    <div class="pub"><time><?= get_the_date() ?></time></div>
    <?php edit_post_link('Edit this post', '<p>', '</p>'); ?>
  </div>
<?php } else { ?>
  <div class="meta">
    <div class='tag-list'>
      <?php echo get_the_tag_list('',', ',''); ?>
    </div>
    <h1 class='post-title'><?php
    if ( is_single() ) :
      the_title();
    else :
      echo '<a href="'.get_permalink().'">'.get_the_title().'</a>';
    endif;
    ?></h1>
    <time><?= get_the_date() ?></time>
  </div>
<?php
  }
}

function uk_limit_char($text, $len) {
  if (strlen($text) < $len) {
    return $text;
  }
  $text_words = explode(' ', $text);
  $out = null;


  foreach ($text_words as $word) {
    if ((strlen($word) > $len) && $out == null) {

        return substr($word, 0, $len) . "...";
    }
    if ((strlen($out) + strlen($word)) > $len) {
        return $out . "...";
    }
    $out.=" " . $word;
  }
  return $out;
}

function uk_numeric_posts_nav() {
  if( is_singular() )
    return;

  global $wp_query;

  /** Stop execution if there's only 1 page */
  if( $wp_query->max_num_pages <= 1 )
    return;

  $paged = get_query_var( 'paged' ) ? absint( get_query_var( 'paged' ) ) : 1;
  $max   = intval( $wp_query->max_num_pages );

  /** Add current page to the array */
  if ( $paged >= 1 )
    $links[] = $paged;

  /** Add the pages around the current page to the array */
  if ( $paged >= 3 ) {
    $links[] = $paged - 1;
    $links[] = $paged - 2;
  }

  if ( ( $paged + 2 ) <= $max ) {
    $links[] = $paged + 2;
    $links[] = $paged + 1;
  }

  echo '<div class="navigation"><ul>' . "\n";

  /** Previous Post Link */
  if ( get_previous_posts_link() )
    printf( '<li>%s</li>' . "\n", get_previous_posts_link() );

  /** Link to first page, plus ellipses if necessary */
  if ( ! in_array( 1, $links ) ) {
    $class = 1 == $paged ? ' class="active"' : '';

    printf( '<li%s><a href="%s">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( 1 ) ), '1' );

    if ( ! in_array( 2, $links ) )
      echo '<li>…</li>';
  }

  /** Link to current page, plus 2 pages in either direction if necessary */
  sort( $links );
  foreach ( (array) $links as $link ) {
    $class = $paged == $link ? ' class="active"' : '';
    printf( '<li%s><a href="%s">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( $link ) ), $link );
  }

  /** Link to last page, plus ellipses if necessary */
  if ( ! in_array( $max, $links ) ) {
    if ( ! in_array( $max - 1, $links ) )
      echo '<li>…</li>' . "\n";

    $class = $paged == $max ? ' class="active"' : '';
    printf( '<li%s><a href="%s">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( $max ) ), $max );
  }

  /** Next Post Link */
  if ( get_next_posts_link() )
    printf( '<li>%s</li>' . "\n", get_next_posts_link() );

  echo '</ul></div>' . "\n";

}


function uk_member() {
  $name = get_sub_field("member_name");
  $image = get_sub_field("member_image");
  if ( $image && ! empty($image) ) :
    $image = $image['sizes']['medium'];
  else :
    $image = "";
  endif;
  $website = get_sub_field("member_website");
  $facebook = get_sub_field("member_facebook");
  $twitter = get_sub_field("member_twitter");
  $google = get_sub_field("member_google");
  $linkedin = get_sub_field("member_linkedin");
  $instagram = get_sub_field("member_instagram");
?>
  <div class='member-jacket'>
    <div class='member'>
      <!-- <img src='/wp&#45;content/themes/union&#45;kitchen/assets/img/square.png' /> -->
      <div class='img' style="background-image:url(<?= $image ?>)"></div>
      <div class='slips'>
        <?php if ( $website ) : ?><div class='slip'>
          <a href="<?= $website ?>" title="Visit Member's Website"><i class="icon-desktop"></i></a>
        </div><?php endif; ?>
        <?php if ( $facebook ) : ?><div class='slip'>
          <a href="<?= $facebook ?>"><i class="icon-facebook-circled-1"></i></a>
        </div><?php endif; ?>
        <?php if ( $twitter ) : ?><div class='slip'>
          <a href="<?= $twitter ?>"><i class="icon-twitter-circled-2"></i></a>
        </div><?php endif; ?>
        <?php if ( $google ) : ?><div class='slip'>
          <a href="<?= $google ?>"><i class="icon-gplus-circled-1"></i></a>
        </div><?php endif; ?>
        <?php if ( $linkedin ) : ?><div class='slip'>
          <a href="<?= $linkedin ?>"><i class="icon-linkedin-circled-1"></i></a>
        </div><?php endif; ?>
        <?php if ( $instagram ) : ?><div class='slip'>
          <a href="<?= $instagram ?>"><i class="icon-instagram"></i></a>
        </div><?php endif; ?>
      </div>
      <h6><?= trim($name) ?></h6>
    </div>
  </div>
<?php
}


add_filter('upload_mimes', 'custom_upload_mimes');
function custom_upload_mimes ( $existing_mimes=array() ) {
  $existing_mimes['zip'] = 'application/zip';
  $existing_mimes['gz'] = 'application/x-gzip';
  $existing_mimes['ai'] = 'application/postscript';
  $existing_mimes['eps'] = 'application/postscript';
  return $existing_mimes;
}

// http://www.advancedcustomfields.com/resources/acf_add_options_sub_page/
if (function_exists('acf_add_options_page')) {
  $general = acf_add_options_page('Site Options');
  acf_add_options_page('Footer');
}

/**
 * Fetching social media
 */
require_once('libs/Twitter.php');
function fetchTwitter($access_token, $token_secret, $consumer_key, $consumer_secret, $count) {
    $twitter = new \Twitter($access_token, $token_secret, $consumer_key, $consumer_secret, $count);
    $twitter->fetch($count);
    return $twitter->data();
}
