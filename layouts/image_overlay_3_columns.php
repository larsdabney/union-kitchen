<div style='margin-bottom:<?= get_sub_field('bottom_margin') ?>px;' class='image_overlay_3_columns corset'>
<?php
while(have_rows('columns')): the_row();
    $image = get_sub_field('image');
    $title = get_sub_field('title');
    $url = get_sub_field('url');
?>
<div class='col' style='background-image:url(<?= $image ?>)'>
        <a href='<?= $url ?>'><span><?= $title ?></span></a>
    </div>

<?php endwhile; ?>
</div>
