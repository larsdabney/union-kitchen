<?php
if (get_sub_field('contained')) {
    $classname = 'text_content contained-corset';
} else {
    $classname = 'corset text_content';
}
?>
<div class='<?= $classname ?>' style='margin-bottom:<?= get_sub_field('bottom_margin') ?>px;'>
    <?= get_sub_field('content') ?>
</div>
