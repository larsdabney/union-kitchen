<?php
$types = get_field_object('field_564f8f3acba34');
$types = $types['choices'];
$members = new WP_Query(array(
    'post_type' => 'member',
    'posts_per_page' => -1,
    'orderby' => 'rand',
    'post_status' => 'publish'
));
$members = $members->get_posts();

$json = array();
foreach($members as $member) :
    $image = get_field('image', $member->ID);
    $image = $image['sizes']['large'];
    $member_types = get_field('type', $member->ID);
    $url = get_field('url', $member->ID);
    $json[] = array(
        'image' => $image,
        'types' => $member_types,
        'url' => $url
    );
endforeach;
echo '<script>window.memberTypes = ' . json_encode(array_keys($types)) . ';</script>';
echo '<script>window.members = ' . json_encode($json) . ';</script>';
?>


<div class='members corset' style='margin-bottom:<?= get_sub_field('bottom_margin') ?>px;'>

    <h1><?= get_sub_field('title') ?></h1>

    <div class='member-types'>

        <?php foreach($types as $key => $val) : ?>

        <button type='button' data-filtername='<?= $key ?>' class='filter <?= $key ?>'><?= $val ?></button>

        <?php endforeach; ?>

        <button type='button' data-filtername='' class=' select-all active'>Select All</button>

        <button type='button' class='deselect-all'>Deselect All</a>

    </div>

    <div class='member-results'></div>

</div>

<script type='text/template' class='member-template'>
    <div class='member-wrap'>
        <div
            style='background-image:url(<%= image %>)'
            class='member <%= types.join(' ') %>'>
            <% if (url) { %>
            <a href='<%= url %>'>
                <span><i class='fa fa-desktop'></i></span>
            </a>
            <% } %>
        </div>
    </div>
</script>
