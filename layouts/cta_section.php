<div class='cta' style='margin-bottom:<?= get_sub_field('bottom_margin') ?>px;background-color:<?= get_sub_field('background_color') ?>;'>
    <h2><?= get_sub_field('title') ?></h2>
    <div>
        <a target="_blank" href='<?= get_sub_field('button_link') ?>' class='btn bordered'><?= get_sub_field('button_label') ?>&nbsp;<i class='fa fa-long-arrow-right'></i></a>
    </div>
</div>

