<?php
/**
 * Basic Twitter interface.
 *
 * @packaged default
 *
 * @see http://dev.lev-interactive.com/home-front-communications/ctfk/blob/staging/wp-content/themes/ctfk/lib/twitter.php
 * @author PS
 **/
class Twitter
{
    public $url = "https://api.twitter.com/1.1/statuses/user_timeline.json";
    public $count = 5;

    public function __construct($access_token, $token_secret, $consumer_key, $consumer_secret)
    {
        $this->oauth_access_token = $access_token;
        $this->oauth_access_token_secret = $token_secret;
        $this->consumer_key = $consumer_key;
        $this->consumer_secret = $consumer_secret;
    }

    private function buildUrlString($baseURI, $method, $params)
    {
        $r = array();
        ksort($params);
        foreach($params as $key=>$value) {
            $r[] = "$key=" . rawurlencode($value);
        }
        return $method."&" . rawurlencode($baseURI) . '&' . rawurlencode(implode('&', $r));
    }

    private function buildAuthorizationHeader($oauth)
    {
        $r = 'Authorization: OAuth ';
        $values = array();
        foreach($oauth as $key=>$value) {
            $values[] = "$key=\"" . rawurlencode($value) . "\"";
        }
        $r .= implode(', ', $values);
        return $r;
    }

    public function fetch($count = null)
    {
        if (null === $count) {
            $count = $this->count;
        }

        if (false !== ($results = get_transient('twitter_feed_'.$count))) {
            $this->data = $results;
        } else {
            $oauth = array('oauth_consumer_key' => $this->consumer_key,
                'oauth_nonce' => time(),
                'oauth_signature_method' => 'HMAC-SHA1',
                'oauth_token' => $this->oauth_access_token,
                'oauth_timestamp' => time(),
                'count' => $count,
                'oauth_version' => '1.0');

            $base_info = $this->buildUrlString($this->url, 'GET', $oauth);
            $composite_key = rawurlencode($this->consumer_secret) . '&' . rawurlencode($this->oauth_access_token_secret);
            $oauth_signature = base64_encode(hash_hmac('sha1', $base_info, $composite_key, true));
            $oauth['oauth_signature'] = $oauth_signature;

            // Make requests
            $header = array($this->buildAuthorizationHeader($oauth), 'Expect:');
            $options = array( CURLOPT_HTTPHEADER => $header,
                //CURLOPT_POSTFIELDS => $postfields,
                CURLOPT_HEADER => false,
                CURLOPT_URL => $this->url."?count=".$count,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_SSL_VERIFYPEER => false);

            $feed = curl_init();
            curl_setopt_array($feed, $options);
            $this->data = curl_exec($feed);
            set_transient('twitter_feed_'.$count, $this->data, MINUTE_IN_SECONDS*10);
            curl_close($feed);
        }
    }

    public function data()
    {
        return json_decode($this->data);
    }
}