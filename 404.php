<?php get_header(); ?>
<section class="corset" id="page-404">
  <div class="center">
    <h1 class="alt page-header">Well, this is embarrassing.</h1>
    <p>The page you were looking for can not be found. Please make your way back to the <a href="/">home page</a>.</p>
  </div>
</div>
<?php get_template_part('part', 'contact'); ?>
<?php get_footer(); ?>
