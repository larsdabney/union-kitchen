(function() {
  jQuery.easing["jswing"] = jQuery.easing["swing"];

  jQuery.extend(jQuery.easing, {
    def: "easeOutQuad",
    swing: function(x, t, b, c, d) {
      return jQuery.easing[jQuery.easing.def](x, t, b, c, d);
    },
    easeInOutCubic: function(x, t, b, c, d) {
      if ((t /= d / 2) < 1) {
        return c / 2 * t * t * t + b;
      }
      return c / 2 * ((t -= 2) * t * t + 2) + b;
    },
    easeOutQuad: function(x, t, b, c, d) {
      return -c * (t /= d) * (t - 2) + b;
    }
  });

  Modernizr.addTest("ipad", function() {
    return !!navigator.userAgent.match(/iPad/i);
  });

  Modernizr.addTest("iphone", function() {
    return !!navigator.userAgent.match(/iPhone/i);
  });

  Modernizr.addTest("ipod", function() {
    return !!navigator.userAgent.match(/iPod/i);
  });

  Modernizr.addTest("appleios", function() {
    return Modernizr.ipad || Modernizr.ipod || Modernizr.iphone;
  });

  $(function() {
    var $doc, $getStarted, $header, PAST_FUN_POINT, PAST_HEADER, headerHeight, onScroll;
    $header = $("header.header");
    if (!$header.is("*")) {
      return;
    }
    $doc = $(document);
    $getStarted = $(".get-started");
    headerHeight = $header.outerHeight();
    PAST_FUN_POINT = false;
    PAST_HEADER = false;
    onScroll = function(e) {
      var screenTop;
      screenTop = $doc.scrollTop();
      if (!PAST_HEADER && screenTop >= headerHeight) {
        PAST_HEADER = true;
        $(".main-nav.sticky").addClass("slideDown");
      } else if (PAST_HEADER && screenTop <= headerHeight) {
        PAST_HEADER = false;
        $(".main-nav.sticky").removeClass("slideDown");
      }
      if (screenTop > 100 && !PAST_FUN_POINT) {
        $getStarted.addClass("hidden");
        return PAST_FUN_POINT = true;
      } else if (screenTop < 100 && PAST_FUN_POINT) {
        $getStarted.removeClass("hidden");
        return PAST_FUN_POINT = false;
      }
    };
    $(window).scroll(onScroll);
    return $(window).resize(function() {
      return headerHeight = $header.outerHeight();
    });
  });

  /*
  $(function() {
    var markers = [];
    var contentWindows = [];
    var latlngs = [];
    var map;
    var bounds = new google.maps.LatLngBounds();
    var icon = {
      path: "M19.9997304,0 C8.54142797,0 0,9.99023232 0,21.5313838 C0,36.1663434 19.9997304,53 19.9997304,53 C19.9997304,53 40,36.1663434 40,21.5313838 C40,10.1454848 31.4580329,0 19.9997304,0 L19.9997304,0 Z",
      fillColor: '#333333',
      fillOpacity: .9,
      anchor: new google.maps.Point(0,0),
      strokeWeight: 0,
      scale: 1
    };
    map = new google.maps.Map($(".get-in-touch .map")[0], {
      zoom: 14,
      center: new google.maps.LatLng(38.908200, -76.926312),
      disableDefaultUI: true,
      draggable: false,
      scrollwheel: false,
      styles: [{"featureType":"landscape","stylers":[{"saturation":-100},{"lightness":65},{"visibility":"on"}]},{"featureType":"poi","stylers":[{"saturation":-100},{"lightness":51},{"visibility":"simplified"}]},{"featureType":"road.highway","stylers":[{"saturation":-100},{"visibility":"simplified"}]},{"featureType":"road.arterial","stylers":[{"saturation":-100},{"lightness":30},{"visibility":"on"}]},{"featureType":"road.local","stylers":[{"saturation":-100},{"lightness":40},{"visibility":"on"}]},{"featureType":"transit","stylers":[{"saturation":-100},{"visibility":"simplified"}]},{"featureType":"administrative.province","stylers":[{"visibility":"off"}]},{"featureType":"water","elementType":"labels","stylers":[{"visibility":"on"},{"lightness":-25},{"saturation":-100}]},{"featureType":"water","elementType":"geometry","stylers":[{"hue":"#ffff00"},{"lightness":-25},{"saturation":-97}]}]
    });

    window.uklocations.forEach(function(loc) {
      latlngs.push(new google.maps.LatLng(loc.location.lat, loc.location.lng));
      markers.push(
        new google.maps.Marker({
          position: latlngs[latlngs.length - 1],
          map: map,
          icon: icon,
          title: loc.title
        })
      );
      contentWindows.push(
        new google.maps.InfoWindow({
          // maxWidth: 200,
          content: '<b>' + loc.title + '</b><br>' + loc.location.address
        })
      );

      (function(marker, win) {
        marker.addListener('click', function() {
          win.open(
            map,
            marker
          );
        });
      })(
        markers[markers.length - 1],
        contentWindows[contentWindows.length - 1]
      );
    });

    function center() {
      var latlngbounds = new google.maps.LatLngBounds();
      latlngs.forEach(function(n){
         latlngbounds.extend(n);
      });
      map.fitBounds(latlngbounds);
    }
    center();
    $(window).resize(center);
  });
  */
  $(function() {
    var removeLoader = function() {
      $("#loader").addClass("removeme");
      return setTimeout((function() {
        return $("#loader").remove();
      }), 705);
    };
    if ($('.page-template-tpl-main').length) {
      setTimeout(removeLoader, 1500);
    } else {
      setTimeout(removeLoader, 1);
    }
    $("a[href*='.jpg'], a[href*='.JPG'], a[href*='.png'], a[href*='.PNG']").fancybox({
      padding: 0,
      helpers: {
        overlay: {
          locked: false
        }
      }
    });
  });

  $(function() {
    var $mobileMenu;

    $mobileMenu = $(".main-nav.sticky")
      .clone()
      .removeClass("sticky")
      .addClass("mobile")
      .append("<a href='javascript:void(0)' class='close-mobile'><i class='icon-cancel-circled'></i></span>")
      .appendTo('.body');

    $("body").on("click", ".tablet-nav-btn", function(e) {
      e.preventDefault();
      return $mobileMenu.addClass("showing");
    });
    return $("body").on("click", ".main-nav.mobile a", function(e) {
      return $mobileMenu.removeClass("showing");
    });
  });

  $(function() {
    var $form, $input, SUBMIT_SCRIPT;
    SUBMIT_SCRIPT = "/wp-content/themes/union-kitchen/uk-subscribe.php";
    $form = $("#subscribe");
    $input = $form.find("input[name='email']");
    return $form.on("submit", function(e) {
      var email;
      e.preventDefault();
      email = $input.val();
      if (/(.+)@(.+){2,}\.(.+){2,}/.test(email)) {
        $form.addClass("disabled");
        return $.post(SUBMIT_SCRIPT, {
          email: email
        }, function(res) {
          $form.removeClass("disabled");
          $input.val("");
          console.log(res);
          if (res && res.message) {
            return alert(res.message);
          }
        });
      } else {
        return alert("Please enter a valid email address.");
      }
    });
  });

  $(function() {
    var $parent = $('.members');
    var tpl;
    var activeClasses;
    var $results;
    var $selectAllBtn;
    var $deselectAllBtn;
    var $buttons;

    if (!$parent.length) {
      return;
    }

    tpl = _.template($('.member-template').html());
    activeClasses = window.memberTypes.slice(0); // Default to all.
    $results = $('.member-results');
    $selectAllBtn = $('.select-all');
    $deselectAllBtn = $('.deselect-all');
    $buttons = $parent.find('.member-types button.filter');

    function setupButtons() {
      $buttons.each(function() {
        if (activeClasses.indexOf($(this).data('filtername')) !== -1) {
          $(this).addClass('active');
        } else {
          $(this).removeClass('active');
        }
      });
    }

    function populateResults() {
      $results.empty();
      window.members.forEach(function(member) {
        var hasType = false;
        member.types.forEach(function(memberType) {
          if (activeClasses.indexOf(memberType) !== -1) {
            hasType = true;
          }
        });
        if (hasType) {
          $results.append(tpl(member));
        }
      });
      setupButtons();
    }

    $selectAllBtn.on('click', function(event) {
      $selectAllBtn.addClass('active');
      activeClasses = window.memberTypes.slice(0);
      populateResults();
      event.preventDefault();
    });

    $deselectAllBtn.on('click', function(event) {
      $selectAllBtn.removeClass('active');
      activeClasses = [];
      populateResults();
      event.preventDefault();
    });

    $buttons.on('click', function(event) {
      var $this = $(this);
      var selectedFilter = $this.data('filtername');
      var index = activeClasses.indexOf(selectedFilter);
      $selectAllBtn.removeClass('active');
      if (index === -1) {
        activeClasses.push(selectedFilter);
      } else {
        activeClasses.splice(index, 1);
      }
      populateResults();
      event.preventDefault();
    });

    populateResults();
    $('.alumni').trigger('click'); // Unselect alumni by default.
  });

  $(function() {
    $('.single-slider').owlCarousel({
      loop: true,
      margin: 10,
      dots: true,
      nav: false,
      center: true,
      items: 1,
      autoplay: 5000,
      autoplaySpeed: 800,
      autoplayTimeout: 5000,
      autoplayHoverPause: true,
    });
  });

  $(function() {
    $('[data-goto]').on('click', function(event) {
      var win;
      var url = $(this).data('goto');
      var tar = $(this).attr('target');

      if (tar) {
        win = window.open(url, tar);
        win.focus();
      } else {
        window.location = url;
      }

      event.preventDefault();
      event.stopPropagation();
    })
    .css('cursor', 'pointer');
  });

}).call(this);
