<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
<?php if ( ! is_single() ) : uk_get_meta(); endif; ?>
  <div class="entry">
      <?php if (is_single() && !is_page()) : ?>
      <h1><?php the_title() ?></h1><br>
      <?php endif; ?>
    <?php the_content() ?>
  </div>
<?php if ( is_single() ) : uk_get_meta(); endif; ?>
</article>

