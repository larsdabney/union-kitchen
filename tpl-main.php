<?php /* Template Name: Main */ ?>
<?php get_header(); ?>

<div class='hp-top-cols'>
	<?php $col_header = get_field('header_text'); ?>
	<div class="col-head-wrap"><?= $col_header ?></div>
<?php
while(have_rows('top_columns')): the_row();
    $image = get_sub_field('image');
    $title = get_sub_field('header');
    $content = get_sub_field('content');
?>

    <div class='col'>
        <!-- <div class='img' style='background-image:url(<?= $image['sizes']['large'] ?>)'></div> -->
        <h2><?= $title ?></h2>
        <div class='bd'><?= $content ?></div>
    </div>
<?php endwhile; ?>
</div>

<div class='hp-membership' style='background-image:url(<?= get_field('member_background_image') ?>)'>
    <div class='bd'>
        <h2>Membership</h2>
        <hr>
        <?= get_field('member_cta_text') ?>
        <div class='membership-callout'>
            <div class='left-side'>
                <h5><?php the_field('callout_box_text'); ?></h5>
            </div>
            <div class='right-side'>
                <a href='<?php the_field('callout_button_url'); ?>' class='btn bordered'>Learn More&nbsp;&nbsp;<i class='fa fa-long-arrow-right'></i></a>
            </div>
        </div>
    </div>
    <div class='overlay' style='background-color:<?= get_field('membership_overlay_color') ?>;'></div>
</div>

<div class='hp-testimonials corset'>
    <h1><?= get_field('testimonial_header') ?></h1>
    <hr>
    <div class='single-slider'>
<?php
while(have_rows('testimonials')): the_row();
    $image = get_sub_field('image');
    $text = get_sub_field('text');
    $byline = get_sub_field('byline');
?>
        <div class='pane item'>
            <div class='img' style='background-image:url(<?= $image['sizes']['large'] ?>)'></div>
            <div class='bd'>
                <div class='text'><?= $text ?></div>
                <div class='byline'><?= $byline ?></div>
            </div>
        </div>
<?php endwhile; ?>
    </div>
</div>

<?php /* ?>
<div class='hp-callout-boxes corset'>
    <h1><?= get_field('callout_boxes_header') ?></h1>
    <hr>
    <div class='boxes'>
<?php
while(have_rows('callout_boxes')): the_row();
    $image = get_sub_field('background_image');
    $text = get_sub_field('text');
    $url = get_sub_field('url');
    $header = get_sub_field('header');
?>
        <div class='box' style='background-image:url(<?= $image['sizes']['large'] ?>)' data-goto='<?= $url ?>'>
            <div class='inner'>
                <div class='inner-aligner'>
                    <h4><?= $header ?></h4>
                    <hr>
                    <div class='text'><?= $text ?></div>
                </div>
            </div>
        </div>
<?php endwhile; ?>
    </div>
</div>
<?php */ ?>

<div class='hp-facts'>
    <div class='corset'>
        <h1><?= get_field('fact_box_header') ?></h1>
        <div class='fact-holder'>
<?php
while(have_rows('facts')): the_row();
    $header = get_sub_field('header');
    $subheader = get_sub_field('sub_header');
?>
            <div class='col'>
                <div class='top-header'><?= $header ?></div>
                <div class='sub-header'><?= $subheader ?></div>
            </div>
            <div class='col-border'></div>
<?php endwhile; ?>
        </div>
    </div>
</div>

<?php

function item($num) {
    $grid_items = get_field('content_grid_blocks');
    $item = $grid_items[$num];
?>
    <div class='item item-<?= $num ?>'>
        <div
            class='piece'
            style='background-image:url(<?= $item['image']['sizes']['large'] ?>)'
            <?= isset($item['external']) && $item['external'] ? 'target="_blank"' : '' ?>
            data-goto='<?= $item['url'] ?>'>
            <div class='block-header'><?= $item['header'] ?></div>
            <div class='overlay'></div>
        </div>
    </div>
<?php
}
?>
<div class='hp-grid'>
    <div class='left-side'>
        <div class='row row-1'>
            <?php item(0); ?>
            <?php item(1); ?>
        </div>
        <div class='row row-2'>
            <?php item(2); ?>
            <?php item(3); ?>
        </div>
        <div class='row row-3'>
            <?php item(4); ?>
            <?php item(5); ?>
        </div>
    </div>
    <div class='right-side'>
        <div class='row row-4'>
            <?php item(6); ?>
        </div>
    </div>
</div>

<?php get_footer(); ?>
